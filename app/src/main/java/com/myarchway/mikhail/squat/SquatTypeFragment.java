package com.myarchway.mikhail.squat;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import com.myarchway.mikhail.squat.data.SquatStorageContract;
import com.myarchway.mikhail.squat.data.SquatStorageHelper;

import java.util.List;

import static android.widget.CursorAdapter.FLAG_AUTO_REQUERY;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link InteractionListener}
 * interface.
 */
public class SquatTypeFragment extends ListFragment {

    private static final String[] ADAPTER_COLUMNS = {SquatStorageContract.SquatType.COLUNM_NAME_TITLE, "percent"};
    private static final int[] ADAPTER_IDS = {R.id.type_title, R.id.type_percent};

    private SquatStorageHelper storageHelper = null;
    private SQLiteDatabase storageDatabase = null;
    private SimpleCursorAdapter adapter = null;

    private InteractionListener mListener;


    public SquatTypeFragment() {
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    /*
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_squat_type_list, container, false);
        return view;
    }
    */

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        getActivity().getMenuInflater().inflate(R.menu.squat_type_list_menu,menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        storageHelper = new SquatStorageHelper(activity.getApplicationContext());

        if (activity instanceof InteractionListener) {
            mListener = (InteractionListener) activity;
        } else {
            throw new RuntimeException(activity.toString()
                    + " must implement OnListFragmentInteractionListener");
        }

        adapter = new SimpleCursorAdapter(activity,R.layout.fragment_squat_type,null,ADAPTER_COLUMNS, ADAPTER_IDS, 0);
        setListAdapter(adapter);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        setListAdapter(null);
        adapter = null;
        storageHelper = null;
        mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();

        if ((storageDatabase==null)||(!storageDatabase.isOpen())) {
            OpenDatabaseTask openDatabaseTask = new OpenDatabaseTask();
            openDatabaseTask.execute();

        }
    }

    @Override
    public void onPause() {
        if (storageDatabase!=null) {
            if (storageDatabase.isOpen()) {
                storageDatabase.close();
                storageDatabase = null;
            }
        }
        super.onPause();
    }

    public interface InteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(Object item);
    }


    private class OpenDatabaseTask extends AsyncTask<Object,Object,SQLiteDatabase> {
        @Override
        protected SQLiteDatabase doInBackground(Object... params) {
            return storageHelper.getReadableDatabase();
        }

        @Override
        protected void onPostExecute(SQLiteDatabase db) {
            super.onPostExecute(db);
            storageDatabase = db;
            refresh();
        }
    }

    public void refresh() {
        if (storageHelper==null) return;
        if (storageDatabase==null) return;
        if (adapter==null) return;
        Cursor cursor = storageHelper.getSquatTypes(storageDatabase);
        Cursor old = adapter.swapCursor(cursor);
        if (old!=null) old.close();
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        Intent intent = new Intent(getActivity(),SquatActivity.class);
        intent.putExtra(SquatActivity.PARAM_SQUAT_TYPE_ID,id);
        startActivity(intent);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add_squat_type:
                addSquatType();
                return true;
            default:
            return super.onOptionsItemSelected(item);
        }
    }

    private void addSquatType() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final EditText input = new EditText(getActivity());
        input.setInputType(InputType.TYPE_CLASS_TEXT );
        input.setHint(R.string.dlg_edit_title_title);
        builder.setView(input);
        builder.setTitle(getString(R.string.dlg_edit_title_title));
        builder.setPositiveButton(R.string.dlg_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                String title = input.getText().toString();
                if (title.length()==0) return;
                if (storageHelper.addSquatType(storageDatabase,title)) {
                    refresh();
                }
            }
        });
        builder.setNegativeButton(R.string.dlg_cancel, null);
        builder.show();
    }
}
