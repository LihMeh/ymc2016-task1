package com.myarchway.mikhail.squat;

import android.app.Application;

//import com.idescout.sql.SqlScoutServer;

/**
 * В принципе, бесполезный класс... Я его создавал только для того, чтобы перегрузить onCreate и запустить SqlScoutServer.
 */

public class SquatApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
//        SqlScoutServer.create(this, getPackageName());
    }
}
