package com.myarchway.mikhail.squat.data;

/**
 * Created by lihmeh on 10/18/16.
 */

public class SquatTypeStats {
    public long id;
    public String title;
    public int record;
    public int today;
    public int currentSession;
}
