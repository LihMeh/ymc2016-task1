package com.myarchway.mikhail.squat.data;

import android.provider.BaseColumns;

/**
 * Created by lihmeh on 10/17/16.
 */

public final class SquatStorageContract {

    private SquatStorageContract() {}

    public final static class SquatType implements BaseColumns {
        public static final String TABLE_NAME = "squat_type";
        public static final String COLUNM_NAME_TITLE = "title";
        public static final String COLUNM_NAME_RECORD = "record";
    }

    /* Подход к приседаниям. В течение каждого дня может быть несколько подходов. Каждый подход - для додного типа приседаний.
    */
    public final static class SquatSet implements BaseColumns {
        public static final String TABLE_NAME = "squat_sets";
        public static final String COLUMN_NAME_SQUAT_TYPE = "squat_type_id";
        public static final String COLUMN_NAME_DATE = "day";
        public static final String COLUMN_NAME_SQUAT_COUNT = "squat_count";
    }

}
