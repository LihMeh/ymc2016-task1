package com.myarchway.mikhail.squat.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.myarchway.mikhail.squat.R;



public class SquatStorageHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 10;
    private static final String DATABASE_NAME = "squats.db";

    private static final String[] DROP_QUERIES = {"drop table if exists "+SquatStorageContract.SquatSet.TABLE_NAME,"drop table if exists "+SquatStorageContract.SquatType.TABLE_NAME};
    private static final String[] CREATE_QUERIES = {
            "create table "+SquatStorageContract.SquatType.TABLE_NAME + " ("
                    + SquatStorageContract.SquatType._ID + " integer primary key"
                    + "," + SquatStorageContract.SquatType.COLUNM_NAME_TITLE + " text not null"
                    + "," + SquatStorageContract.SquatType.COLUNM_NAME_RECORD + " integer not null default 30"
                     +")",
            "create table "+SquatStorageContract.SquatSet.TABLE_NAME + " ("
                    + SquatStorageContract.SquatSet._ID + " integer primary key"
                    + "," + SquatStorageContract.SquatSet.COLUMN_NAME_DATE + " date not null"
                    + "," + SquatStorageContract.SquatSet.COLUMN_NAME_SQUAT_TYPE + " integer not null"
                    + "," + SquatStorageContract.SquatSet.COLUMN_NAME_SQUAT_COUNT + " integer not null"
                    + ", foreign key ("+ SquatStorageContract.SquatSet.COLUMN_NAME_SQUAT_TYPE +") references "+SquatStorageContract.SquatType.TABLE_NAME+"("+SquatStorageContract.SquatType._ID+")"
                    +")"
    };

    private Context context = null;

    public SquatStorageHelper(Context context) {
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        execAll(db,CREATE_QUERIES);
        defaultSquats(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Не поддерживаю апгрейд. Заглушка, чтобы метод корректно работал. При изменении структуры БД, добавить корректный апгрейд!
        execAll(db, DROP_QUERIES);
        onCreate(db);
    }

    private void execAll(SQLiteDatabase db, String[] queries) {
        for (String query: queries ){
            db.execSQL(query);
        }
    }

    private void defaultSquats(SQLiteDatabase db) {
        int[] squatTypeNames = {R.string.default_squat_type_0, R.string.default_squat_type_1, R.string.default_squat_type_2};
        db.beginTransaction();
        for (int stringRes: squatTypeNames) {
            ContentValues values = new ContentValues();
            values.put(SquatStorageContract.SquatType.COLUNM_NAME_TITLE, context.getString(stringRes));
            db.insert(SquatStorageContract.SquatType.TABLE_NAME,null,values);
        }
        db.setTransactionSuccessful();
        db.endTransaction();
    }

    public SquatTypeStats getSquatTypeStats (SQLiteDatabase db, long id) {
        String[] idString = new String[]{String.valueOf(id)};
        SquatTypeStats stats = new SquatTypeStats();

        Cursor cursor = db.query(SquatStorageContract.SquatType.TABLE_NAME,
                new String[]{SquatStorageContract.SquatType.COLUNM_NAME_TITLE
                            ,SquatStorageContract.SquatType.COLUNM_NAME_RECORD
                            },
                "_id = ?",
                idString,null,null,null);
        if (!cursor.moveToFirst()) return null;
        stats.id = id;
        stats.title = cursor.getString(0);
        stats.record = cursor.getInt(1);
        cursor.close();

        Cursor squatsTodayCursor = db.rawQuery("select sum( "+SquatStorageContract.SquatSet.COLUMN_NAME_SQUAT_COUNT+" ) from "+ SquatStorageContract.SquatSet.TABLE_NAME+" where "+ SquatStorageContract.SquatSet.COLUMN_NAME_SQUAT_TYPE +" = ? and date("+ SquatStorageContract.SquatSet.COLUMN_NAME_DATE+") = date('now','localtime')",
                idString);
        if (!squatsTodayCursor.moveToFirst()) {
            squatsTodayCursor.close();
            return null;
        }
        stats.today = squatsTodayCursor.getInt(0);
        squatsTodayCursor.close();

        return stats;
    }

    public void saveSquatSet (SQLiteDatabase db, long squatTypeId, SquatTypeStats stats) {
        if (stats==null) return;
        if (stats.currentSession<1) return;

        //// Попытки сделать по-человечески оказались безуспешны: значение date('now') неизбежно экранируется  и в базу данных пишется строкой 'как есть".
//        ContentValues values = new ContentValues();
//        values.put(SquatStorageContract.SquatSet.COLUMN_NAME_DATE,"date('now')");
//        values.put(SquatStorageContract.SquatSet.COLUMN_NAME_SQUAT_TYPE ,squatTypeId);
//        values.put(SquatStorageContract.SquatSet.COLUMN_NAME_SQUAT_COUNT,squatCount);
//        long a = db.insert(SquatStorageContract.SquatSet.TABLE_NAME,null,values);

        db.beginTransaction();

        db.execSQL("insert into "+SquatStorageContract.SquatSet.TABLE_NAME+" ("+SquatStorageContract.SquatSet.COLUMN_NAME_DATE+", "+SquatStorageContract.SquatSet.COLUMN_NAME_SQUAT_TYPE+", "+SquatStorageContract.SquatSet.COLUMN_NAME_SQUAT_COUNT+") values (date('now','localtime'), ?, ?)",new Object[]{Long.valueOf(squatTypeId), Integer.valueOf(stats.currentSession)});

        // Результат не проверяю! Ай-яй!

        // теперь обновим рекорд (если нужно)
        if (stats.today+stats.currentSession > stats.record) {
            ContentValues values = new ContentValues();
            values.put(SquatStorageContract.SquatType.COLUNM_NAME_RECORD,stats.today+stats.currentSession);
            db.update(SquatStorageContract.SquatType.TABLE_NAME,values,SquatStorageContract.SquatType._ID+" = ?",new String[]{String.valueOf(stats.id)});
        }

        db.setTransactionSuccessful();
        db.endTransaction();


    }

    public void saveSquatTypeTitle (SQLiteDatabase db, long squatTypeId, String newTitle) {
        ContentValues values = new ContentValues();
        values.put(SquatStorageContract.SquatType.COLUNM_NAME_TITLE,newTitle);
        db.update(SquatStorageContract.SquatType.TABLE_NAME,values,SquatStorageContract.SquatType._ID+" = ?",new String[]{String.valueOf(squatTypeId)});
    }

    public Cursor getSquatTypes(SQLiteDatabase db) {
        // TODO: Психанул,  забил на Contract, написал запрос так. Исправить, если будет время
        return db.rawQuery("select T._id as _id, T.title as title, (cast((100*sum(S.squat_count)/T.record) as int) || '%') as percent from squat_type T left join squat_sets S on S.squat_type_id = T._id and S.day = date('now') group by (T._id) ",null);
    }

    public void deleteSquatType(SQLiteDatabase db, long squatTypeId) {
        db.delete(SquatStorageContract.SquatType.TABLE_NAME, SquatStorageContract.SquatType._ID + " = ?",new String[]{String.valueOf(squatTypeId)});
    }

    public boolean addSquatType(SQLiteDatabase db, String title) {
        ContentValues values = new ContentValues();
        values.put(SquatStorageContract.SquatType.COLUNM_NAME_TITLE,title);
        return db.insert(SquatStorageContract.SquatType.TABLE_NAME,null,values) != -1;
    }

}
