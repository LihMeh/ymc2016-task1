package com.myarchway.mikhail.squat;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.myarchway.mikhail.squat.data.SquatStorageHelper;
import com.myarchway.mikhail.squat.data.SquatTypeStats;


public class SquatActivity extends AppCompatActivity {

    public static final String PARAM_SQUAT_TYPE_ID = "param_squat_type_id";

    private long squatTypeId = 0;

    private SquatStorageHelper storageHelper = null;
    private SQLiteDatabase storageDatabase = null;
    SquatTypeStats stats = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getIntent().getExtras() != null) {
            squatTypeId = getIntent().getExtras().getLong(PARAM_SQUAT_TYPE_ID,0);
        }

        setContentView(R.layout.activity_squat);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        storageHelper = new SquatStorageHelper(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.squat_activity_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onResume() {
        super.onResume();
        storageDatabase = storageHelper.getWritableDatabase();
        if (storageDatabase==null) finish();
        if (!loadData()) finish();
        showData();
    }

    @Override
    protected void onPause() {
        saveData();
        storageDatabase = null;
        super.onPause();
    }

    private boolean loadData() {
        stats = storageHelper.getSquatTypeStats(storageDatabase, squatTypeId);
        if (stats==null) {
            return false;
        }
        showData();
        return true;
    }

    private void saveData() {
        if (stats.currentSession>0) {
            storageHelper.saveSquatSet(storageDatabase, stats.id, stats);
        }
    }

    private void showData() {
        if (stats==null) return;
        setTitle(stats.title);
        ((TextView)findViewById(R.id.recordCounter)).setText(String.valueOf(stats.record));
        ((TextView)findViewById(R.id.todayCounter)).setText(String.valueOf(stats.today+stats.currentSession));
        int progress = 100;
        if ((stats.today+stats.currentSession)<stats.record) {
            progress = 100*(stats.today+stats.currentSession)/stats.record;
        }
        ((ProgressBar)findViewById(R.id.progressBar)).setProgress(progress);
    }

    public void onBtnClick(View view) {
        stats.currentSession++;
        showData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_clear_session:
                clearCurrectSession();
                return true;
            case R.id.action_edit_title:
                editTitle();
                return true;
            case R.id.action_delete_type:
                deleteType();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void clearCurrectSession() {
        stats.currentSession = 0;
        showData();
    }

    private void editTitle() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_TEXT );
        input.setText(stats.title);
        builder.setView(input);
        builder.setTitle(getString(R.string.dlg_edit_title_title));
        builder.setPositiveButton(R.string.dlg_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                String newTitle = input.getText().toString();
                storageHelper.saveSquatTypeTitle(storageDatabase,stats.id,newTitle);
                setTitle(newTitle);
            }
        });
        builder.setNegativeButton(R.string.dlg_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        });
        builder.show();
    }

    private void deleteType() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.dlg_delete);
        builder.setPositiveButton(R.string.dlg_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                storageHelper.deleteSquatType(storageDatabase,stats.id);
                finish();
            }
        });
        builder.setNegativeButton(R.string.dlg_cancel, null);
        builder.show();
    }
}
